package org.fjala.mitzi.bookindex;

import org.fjala.mitzi.bookindex.core.RedBlackTree;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class Term {
    private final RedBlackTree<Integer, Integer> pages;
    private String term;

    public Term(String term) {
        this.term = term;
        this.pages = new RedBlackTree<>();
    }

    public void insertPage(int page) {
        pages.insert(page, page);
    }

    public String getTerm() {
        return term;
    }

    protected void setTerm(String term) {
        this.term = term;
    }

    @Override
    public String toString() {
        return term + ": " + pagesAsString();
    }

    private String pagesAsString() {
        AtomicReference<Integer> previous = new AtomicReference<>();
        AtomicBoolean staging = new AtomicBoolean();
        StringBuilder builder = new StringBuilder();
        pages.forEachInOrder(node -> {
            int current = node.getKey();
            if (previous.get() != null) {
                if (previous.get() + 1 == current) {
                    if (!staging.get()) {
                        staging.set(true);
                        builder.append(previous);
                        builder.append(" - ");
                    }
                } else {
                    staging.set(false);
                    builder.append(previous).append(", ");
                }
            }
            previous.set(current);
        });
        builder.append(previous);
        return builder.toString();
    }

    public RedBlackTree<Integer, Integer> getPages() {
        return pages;
    }

    public List<Integer> getPagesAsList() {
        LinkedList<Integer> pagesAsList = new LinkedList<>();
        pages.forEachInOrder(node -> pagesAsList.add(node.getKey()));
        return pagesAsList;
    }
}
