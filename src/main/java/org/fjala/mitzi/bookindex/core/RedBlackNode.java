package org.fjala.mitzi.bookindex.core;

import java.util.function.Consumer;

public class RedBlackNode<T, K extends Comparable<K>> {
    public static final boolean RED = true;
    public static final boolean BLACK = false;
    protected T value;
    protected K key;
    protected RedBlackNode<T, K> left;
    protected RedBlackNode<T, K> right;
    protected RedBlackNode<T, K> parent;
    protected boolean isRed;

    public RedBlackNode(T value, K key, RedBlackNode<T, K> left, RedBlackNode<T, K> right, RedBlackNode<T, K> parent) {
        this.value = value;
        this.key = key;
        this.left = left;
        this.right = right;
        if (left != null)
            this.left.parent = this;
        if (left != null)
            this.right.parent = this;
        this.parent = parent;
    }

    public static <T, K extends Comparable<K>> RedBlackNode<T, K> newNil() {
        return new RedBlackNode<>(null, null, null, null, null);
    }

    @Override
    public String toString() {
        if (value == null) {
            return "NIL";
        }
        return value.toString();
    }

    public T getValue() {
        return value;
    }

    public K getKey() {
        return key;
    }

    public RedBlackNode<T, K> getLeft() {
        return left;
    }

    public RedBlackNode<T, K> getRight() {
        return right;
    }

    public RedBlackNode<T, K> getParent() {
        return parent;
    }

    public boolean isRed() {
        return isRed;
    }

    public void setColor(boolean color) {
        this.isRed = color;
    }

    public boolean isBlack() {
        return !isRed;
    }

    public void preOrder(Consumer<RedBlackNode<T, K>> consumer) {
        preOrder(this, consumer);
    }

    private void preOrder(RedBlackNode<T, K> node, Consumer<RedBlackNode<T, K>> consumer) {
        if (node == null || node.value == null) {
            return;
        }
        consumer.accept(node);
        preOrder(node.left, consumer);
        preOrder(node.right, consumer);
    }
}
