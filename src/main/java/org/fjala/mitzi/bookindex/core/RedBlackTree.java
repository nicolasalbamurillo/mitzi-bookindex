package org.fjala.mitzi.bookindex.core;

import java.util.function.Consumer;

import static org.fjala.mitzi.bookindex.core.RedBlackNode.*;

public class RedBlackTree<T, K extends Comparable<K>> {
    private final RedBlackNode<T, K> nil;
    private RedBlackNode<T, K> root;

    public RedBlackTree() {
        this(newNil());
    }

    public RedBlackTree(RedBlackNode<T, K> nil) {
        this(nil, nil);
    }

    public RedBlackTree(RedBlackNode<T, K> root, RedBlackNode<T, K> nil) {
        this.root = root;
        this.nil = nil;
    }

    public void rotateLeft(RedBlackNode<T, K> x) {
        RedBlackNode<T, K> y = x.right;
        x.right = y.left;
        if (y.left != nil) {
            y.left.parent = x;
        }
        y.parent = x.parent;
        if (x.parent == nil) {
            root = y;
        } else if (x.parent.left == x) {
            x.parent.left = y;
        } else {
            x.parent.right = y;
        }
        y.left = x;
        x.parent = y;
    }

    public void rotateRight(RedBlackNode<T, K> y) {
        RedBlackNode<T, K> x = y.left;
        y.left = x.right;
        if (x.right != nil) {
            x.right.parent = y;
        }
        x.parent = y.parent;
        if (y.parent == nil) {
            root = x;
        } else if (y.parent.left == y) {
            y.parent.left = x;
        } else {
            y.parent.right = x;
        }
        x.right = y;
        y.parent = x;
    }

    public RedBlackNode<T, K> getRoot() {
        return root;
    }

    public void insert(T value, K key) {
        RedBlackNode<T, K> node = new RedBlackNode<>(value, key, nil, nil, nil);
        insert(node);
    }

    public void insert(RedBlackNode<T, K> node) {
        if (root == nil) {
            root = node;
            return;
        }
        root = insert(root, node, nil);
        fixAfterInsert(node);
    }

    private RedBlackNode<T, K> insert(RedBlackNode<T, K> tree, RedBlackNode<T, K> node, RedBlackNode<T, K> parent) {
        if (tree == nil) {
            node.parent = parent;
            node.left = nil;
            node.right = nil;
            node.isRed = true;
            return node;
        }
        int cmp = node.key.compareTo(tree.key);
        if (cmp < 0) {
            tree.left = insert(tree.left, node, tree);
        } else {
            tree.right = insert(tree.right, node, tree);
        }
        return tree;
    }

    private void fixAfterInsert(RedBlackNode<T, K> x) {
        while (x.parent.isRed) {
            if (x.parent == x.parent.parent.left) {
                RedBlackNode<T, K> y = x.parent.parent.right;
                if (y.isRed) {
                    x.parent.isRed = false;
                    y.isRed = false;
                    x.parent.parent.isRed = true;
                    x = x.parent.parent;
                } else {
                    if (x == x.parent.right) {
                        x = x.parent;
                        rotateLeft(x);
                    }
                    x.parent.isRed = false;
                    x.parent.parent.isRed = true;
                    rotateRight(x.parent.parent);
                }
            } else {
                RedBlackNode<T, K> y = x.parent.parent.left;
                if (y.isRed) {
                    x.parent.isRed = false;
                    y.isRed = false;
                    x.parent.parent.isRed = true;
                    x = x.parent.parent;
                } else {
                    if (x == x.parent.left) {
                        x = x.parent;
                        rotateRight(x);
                    }
                    x.parent.isRed = false;
                    x.parent.parent.isRed = true;
                    rotateLeft(x.parent.parent);
                }
            }
        }
        root.isRed = false;
    }

    public RedBlackNode<T, K> getNil() {
        return nil;
    }

    public T get(K key) {
        RedBlackNode<T, K> result = getNode(root, key);
        return result == null ? null : result.value;
    }

    private RedBlackNode<T, K> getNode(RedBlackNode<T, K> tree, K keyToSearch) {
        if (tree == nil) {
            return null;
        }
        int compare = tree.getKey().compareTo(keyToSearch);
        if (compare == 0) {
            return tree;
        } else if (compare > 0) {
            return getNode(tree.left, keyToSearch);
        } else {
            return getNode(tree.right, keyToSearch);
        }
    }

    private void transplant(RedBlackNode<T, K> x, RedBlackNode<T, K> y) {
        if (x.parent == nil) {
            root = y;
        } else if (x == x.parent.left) {
            x.parent.left = y;
        } else {
            x.parent.right = y;
        }
        y.parent = x.parent;
    }

    public T delete(K key) {
        RedBlackNode<T, K> node = getNode(root, key);
        if (node != null) {
            delete(node);
        }
        return node.value;
    }

    private void delete(RedBlackNode<T, K> x) {
        RedBlackNode<T, K> y = x;
        RedBlackNode<T, K> z;
        boolean yPreviousColor = y.isRed;
        if (x.left == nil) {
            z = x.right;
            transplant(x, x.right);
        } else if (x.right == nil) {
            z = x.left;
            transplant(x, x.left);
        } else {
            y = min(x.right);
            yPreviousColor = y.isRed;
            z = y.right;
            if (y.parent == x) {
                z.parent = y;
            } else {
                transplant(y, y.right);
                y.right = x.right;
                y.right.parent = y;
            }
            transplant(x, y);
            y.left = x.left;
            y.left.parent = y;
            y.isRed = x.isRed;
        }
        if (yPreviousColor == RedBlackNode.BLACK) {
            fixAfterDelete(z);
        }
    }

    private void fixAfterDelete(RedBlackNode<T, K> x) {
        while (x != root && x.isBlack()) {
            if (x == x.parent.left) {
                RedBlackNode<T, K> w = x.parent.right;
                if (w.isRed()) {
                    w.isRed = BLACK;
                    x.parent.isRed = RED;
                    rotateLeft(x.parent);
                    w = x.parent.right;
                }
                if (w.left.isBlack() && w.right.isBlack()) {
                    w.isRed = RED;
                    x = x.parent;
                } else {
                    if (w.right.isBlack()) {
                        w.left.isRed = BLACK;
                        w.isRed = RED;
                        rotateRight(w);
                        w = x.parent.right;
                    }
                    w.isRed = x.parent.isRed;
                    x.parent.isRed = BLACK;
                    w.right.isRed = BLACK;
                    rotateLeft(x.parent);
                    x = root;
                }
            } else {
                RedBlackNode<T, K> w = x.parent.left;
                if (w.isRed()) {
                    w.isRed = BLACK;
                    x.parent.isRed = RED;
                    rotateRight(x.parent);
                    w = x.parent.left;
                }
                if (w.right.isBlack() && w.left.isBlack()) {
                    w.isRed = RED;
                    x = x.parent;
                } else {
                    if (w.left.isBlack()) {
                        w.right.isRed = BLACK;
                        w.isRed = RED;
                        rotateLeft(w);
                        w = x.parent.left;
                    }
                    w.isRed = x.parent.isRed;
                    x.parent.isRed = BLACK;
                    w.right.isRed = BLACK;
                    rotateRight(x.parent);
                    x = root;
                }
            }
        }
        x.isRed = BLACK;
    }

    private RedBlackNode<T, K> min(RedBlackNode<T, K> tree) {
        if (tree.left == nil)
            return tree;
        return min(tree.left);
    }

    public void forEachInOrder(Consumer<RedBlackNode<T, K>> consumer) {
        forEachInOrder(root, consumer);
    }

    private void forEachInOrder(RedBlackNode<T, K> node, Consumer<RedBlackNode<T, K>> consumer) {
        if (node == null || node == nil) {
            return;
        }
        forEachInOrder(node.left, consumer);
        consumer.accept(node);
        forEachInOrder(node.right, consumer);
    }

    @Override
    public String toString() {
        return toString(", ");
    }

    public String toString(String separator) {
        final StringBuilder sb = new StringBuilder();
        addIntoBuilder(root, sb, separator);
        sb.delete(sb.length() - separator.length(), sb.length());
        return sb.toString();
    }

    private void addIntoBuilder(RedBlackNode<T, K> tree, StringBuilder sb, String separator) {
        if (tree.left != null)
            addIntoBuilder(tree.left, sb, separator);
        sb.append(tree.key).append(separator);
        if (tree.right != null)
            addIntoBuilder(tree.right, sb, separator);
    }

    public boolean search(K key) {
        return get(key) != null;
    }
}