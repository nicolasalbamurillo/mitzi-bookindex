package org.fjala.mitzi.bookindex;

import org.fjala.mitzi.bookindex.core.RedBlackTree;

import java.util.ArrayList;
import java.util.Collection;

import static org.fjala.mitzi.bookindex.utils.StringUtils.starsWithIgnoreCase;

public class BookIndex {
    private final RedBlackTree<Term, String> terms;

    public BookIndex() {
        this.terms = new RedBlackTree<>();
    }

    public void addTerm(String termKey, int page) {
        Term term = terms.get(termKey);
        if (term == null) {
            term = new Term(termKey);
            terms.insert(term, termKey);
        }
        term.insertPage(page);
    }

    public void deleteTerm(String term) {
        terms.delete(term);
    }

    public void updateTerm(String term, String newName) {
        Term deletedTerm = terms.delete(term);
        if (deletedTerm != null) {
            deletedTerm.setTerm(newName);
            terms.insert(deletedTerm, newName);
        }
    }

    public Collection<String> getTermsStarsWith(String starsWith) {
        Collection<String> matchedTerms = new ArrayList<>();
        terms.forEachInOrder(term -> {
            if (starsWithIgnoreCase(term.getKey(), starsWith)) {
                matchedTerms.add(term.getKey());
            }
        });
        return matchedTerms;
    }


    public boolean hasTerm(String term) {
        return terms.search(term);
    }

    public Term getTerm(String keyTerm) {
        return terms.get(keyTerm);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        terms.forEachInOrder(node -> {
            if (node != terms.getNil())
                sb.append(node.getValue()).append('\n');
        });
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
