package org.fjala.mitzi.bookindex.utils;

public class StringUtils {
    public static boolean starsWithIgnoreCase(String toSearch, String starsWith) {
        return toSearch.toLowerCase().startsWith(starsWith.toLowerCase());
    }
}
