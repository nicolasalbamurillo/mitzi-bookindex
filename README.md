# Mitzi Book Index

Nombre: Nicolas Alba Murillo

## Explicacion de la estructura:
Ya que uno de los principales requerimientos era que el book index es que se muestre ordenado, tanto en sus terminos como en
las paginas de sus terminos, la mejor opcion es un RedBlackTree, ya que tanto tiene ventajas en la busqueda y ordenacion de los elementos.

- Esta siempre **aproximadamente balanceado** despues del CRUD, lo que permite una busqueda mas eficiente que un BST.
- Es facil de recorrer los elementos en orden de menor a mayor atraves de un recorrido inorder(left - root - right).
- Las operaciones de Search, Delete, Insert, estan en una complejidad de **"O(Log n)"**. Siendo mas eficiente que muchas otras estructucas de datos. 

Para Este problema usamos dos RedBlackTree, primero cada Termino tiene un RedBlackTree para sus paginas,
despues el Book index tiene un RedBlackTree de terminos. De esta manera mantedremos tanto paginas de terminos como terminos ordenados en el bookindex de manera eficiente.

## Algoritmos y operaciones del RedBlackTree
Los algoritmos y operaciones del RedBlackTree fueron los que aprendimos en las clase, con algunas ligeras modificaciones.
Sobre todo en el toString de la Clase Term.